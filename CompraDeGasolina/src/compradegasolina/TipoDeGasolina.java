package compradegasolina;

//tipos de gasolina que hay dentro de la empresa
public class TipoDeGasolina {
    private String Nombre;
    private String Apellido;
    private int Cedula;
    private String Direccion;
    private int GasolinaSuper;
    private Double GasolinaExtra;
    private int CantidadDeGalones;
    private int Iva;
    private double Subtotal;
    


     public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getCedula() {
        return Cedula;
    }

    public void setCedula(int Cedula) {
        this.Cedula = Cedula;
    }

    public String getDireccion() {
        return Direccion;
    }

    public void setDireccion(String Direccion) {
        this.Direccion = Direccion;
    }
        public int getGasolinaSuper() {
        return GasolinaSuper;
    }

    public void setGasolinaSuper() {
        this.GasolinaSuper = 2;
    }

    public Double getGasolinaExtra() {
        return GasolinaExtra;
    }

    public void setGasolinaExtra() {
        this.GasolinaExtra = 1.50;
    }

    public int getCantidadDeGalones() {
        return CantidadDeGalones;
    }

    public void setCantidadDeGalones(int CantidadDeGalones) {
        this.CantidadDeGalones = CantidadDeGalones;
    }

    public int getIva() {
        return Iva;
    }

    public void setIva() {
        this.Iva = 2/100;
    }
    

}
